/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2024-2025 Free Software Foundation, Inc.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <string.h>
#include <mailutils/cstr.h>

int
mu_string_suffix (char const *str, char const *sfx)
{
  size_t sfxlen = strlen (sfx);
  size_t len = strlen (str); 
  if (len < sfxlen)
    return 0;
  return memcmp (str + len - sfxlen, sfx, sfxlen) == 0;
}
