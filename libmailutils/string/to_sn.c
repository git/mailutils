/* GNU Mailutils -- a suite of utilities for electronic mail
   Copyright (C) 2002-2025 Free Software Foundation, Inc.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef STR_TO_FUN
# error "STR_TO_FUN not defined"
#endif
#ifndef STR_TO_TYPE
# error "STR_TO_TYPE not defined"
#endif
#ifndef STR_TO_MIN
# error "STR_TO_MIN not defined"
#endif
#ifndef STR_TO_MAX
# error "STR_TO_MAX not defined"
#endif

static int
STR_TO_FUN (void *tgt, char const *string, char **errmsg)
{
  STR_TO_TYPE *ptr = tgt;
  intmax_t v;
  char *p;
  
  errno = 0;
  v = strtoimax (string, &p, 10);
  if (errno)
    return errno;
  if (*p)
    return EINVAL;
  if (STR_TO_MIN <= v && v <= STR_TO_MAX)
    {
      *ptr = (STR_TO_TYPE) v;
      return 0;
    }
  return ERANGE;
}
#undef STR_TO_FUN
#undef STR_TO_TYPE
#undef STR_TO_MIN
#undef STR_TO_MAX
